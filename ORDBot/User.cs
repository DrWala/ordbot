﻿using Microsoft.Bot.Connector;
using ORDBot.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORDBot
{
    public class User
    {
        public string UniqueId { get; set; }
        public Activity UserAcitivity { get; set; }
        public DateTime ORDDate { get; set; }
        public string DaysLeftToORD
        {
            get
            {
                return (ORDDate - DateTime.Now).Days.ToString();
            }
        }
        public string DaysToReservist { get; set; } = (new Random().Next(7) + 1).ToString();
        public static bool CheckIfUserExist(string uniqueId)
        {
            return GlobalConfig.UserList.Exists(u => u.UniqueId == uniqueId);
        }
        public static User GetUserById(string uniqueId)
        {
            return GlobalConfig.UserList.Find(u => u.UniqueId == uniqueId);
        }
    }

}
