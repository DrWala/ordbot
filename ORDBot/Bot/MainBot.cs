﻿using Microsoft.Bot.Connector;
using ORDBot.Config;
using ORDBot.Diagnostic;
using ORDBot.Firebase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORDBot.Bot
{
    public class MainBot
    {
        public static async Task<string> GenerateResponse(Activity activityToProcess)
        {
            ORDDiagnostic.SendDiagnostic("Text: " + activityToProcess.Text);
            string response = "";
            if (!User.CheckIfUserExist(activityToProcess.From.Id))
            {
                try
                {
                    if (activityToProcess.Text == "/start" || activityToProcess.Text == "/start start")
                        return response = "Welcome.";
                    DateTime ordDate = Convert.ToDateTime(activityToProcess.Text);
                    GlobalConfig.UserList.Add(new User
                    {
                        UniqueId = activityToProcess.From.Id,
                        UserAcitivity = activityToProcess,
                        ORDDate = ordDate
                    });
                    FirebaseDataManager.UpdateAllUsersArr();
                    response = "Ah gong Lee is pleased to have you on board";
                }
                catch (FormatException fex)
                {
                    response = "Your date was WRONG! Extending your ORD date. Also guard duty next weekend.";
                    User.GetUserById(activityToProcess.From.Id).ORDDate.AddDays(new Random().Next(20) + 1);
                    ORDDiagnostic.SendDiagnostic(fex);
                }
                catch (Exception ex)
                {
                    response = "Something went wrong. Extending your ORD date...";
                    User.GetUserById(activityToProcess.From.Id).ORDDate.AddDays(new Random().Next(20) + 1);
                    ORDDiagnostic.SendDiagnostic(ex);
                }
            }
            else
            {
                User.GetUserById(activityToProcess.From.Id).ORDDate.AddDays(new Random().Next(20) + 1);
                response = "Messaging me without prior approval from Ah Gong has resulted in your ORD date being extended. Your ORD in " + User.GetUserById(activityToProcess.From.Id).DaysLeftToORD + " days";
            }
            return response;
        }
        public static async void Annoy()
        {

            while (true)
            {
                System.Threading.Thread.Sleep(1000 * 15/*new Random().Next(100)*/);
                try
                {
                    await ORDDiagnostic.SendDiagnostic("Count: " + GlobalConfig.UserList.Count);
                    for (int index = 0; index < GlobalConfig.UserList.Count; index++)
                    {
                        await ORDDiagnostic.SendDiagnostic("Current count: " + index);
                        try
                        {
                            await ORDDiagnostic.SendDiagnostic("Annoying now. User: " + GlobalConfig.UserList[index].UserAcitivity.From.Name);
                            ConnectorClient connector = new ConnectorClient(new Uri(GlobalConfig.UserList[index].UserAcitivity.ServiceUrl));
                            if (Convert.ToInt32(GlobalConfig.UserList[index].DaysLeftToORD) > 0)
                            {
                                User u = GlobalConfig.UserList[index];
                                int random = new Random().Next(100);
                                string quote = GlobalConfig.QuotesDuring[new Random().Next(GlobalConfig.QuotesDuring.Count)];
                                quote = quote.Replace("{0}+{random}", (Convert.ToInt32(u.DaysLeftToORD) + random).ToString());
                                GlobalConfig.UserList[index].ORDDate.AddDays(random);

                                quote = quote.Replace("{0}", GlobalConfig.UserList[index].DaysLeftToORD);
                                Activity reply = GlobalConfig.UserList[index].UserAcitivity.CreateReply(quote);
                                await connector.Conversations.ReplyToActivityAsync(reply);
                            }
                            else
                            {
                                User u = GlobalConfig.UserList[index];
                                int random = new Random().Next(100);
                                string quote = GlobalConfig.QuotesAfter[new Random().Next(GlobalConfig.QuotesDuring.Count)];


                                quote = quote.Replace("{randomReservist}", (Convert.ToInt32(new Random().Next(7) + 1).ToString()));
                                quote = quote.Replace("{0}+{random}", (Convert.ToInt32(u.DaysLeftToORD) + random).ToString());
                                GlobalConfig.UserList[index].ORDDate.AddDays(random);

                                quote = quote.Replace("{0}", GlobalConfig.UserList[index].DaysLeftToORD);
                                Activity reply = GlobalConfig.UserList[index].UserAcitivity.CreateReply(quote);
                                await connector.Conversations.ReplyToActivityAsync(reply);
                            }
                            System.Threading.Thread.Sleep(1000 * 1/*new Random().Next(100)*/);
                        }
                        catch (Exception ex)
                        {
                            await ORDDiagnostic.SendDiagnostic(ex);
                        }

                    }

                    await FirebaseDataManager.UpdateAllUsersArr();
                    await FirebaseDataManager.GetAllUsers();
                }
                catch (Exception ex)
                {
                    await ORDDiagnostic.SendDiagnostic(ex);
                }

                //try
                //{
                //    Parallel.For(0, GlobalConfig.UserList.Count, index =>
                //    {
                //        ORDDiagnostic.SendDiagnostic("Annoying now.");
                //        lock (GlobalConfig.UserList)
                //        {
                //            ConnectorClient connector = new ConnectorClient(new Uri(GlobalConfig.UserList[index].UserAcitivity.ServiceUrl));
                //            if (Convert.ToInt32(GlobalConfig.UserList[index].DaysLeftToORD) > 0)
                //            {
                //                User u = GlobalConfig.UserList[index];
                //                int random = new Random().Next(100);
                //                string quote = GlobalConfig.QuotesDuring[new Random().Next(GlobalConfig.QuotesDuring.Count)];
                //                quote = quote.Replace("{0}+{random}", (Convert.ToInt32(u.DaysLeftToORD) + random).ToString());
                //                GlobalConfig.UserList[index].ORDDate.AddDays(random);

                //                quote = quote.Replace("{0}", GlobalConfig.UserList[index].DaysLeftToORD);
                //                Activity reply = GlobalConfig.UserList[index].UserAcitivity.CreateReply(quote);
                //                connector.Conversations.ReplyToActivity(reply);
                //            }
                //            else
                //            {
                //                User u = GlobalConfig.UserList[index];
                //                int random = new Random().Next(100);
                //                string quote = GlobalConfig.QuotesAfter[new Random().Next(GlobalConfig.QuotesDuring.Count)];


                //                quote = quote.Replace("{randomReservist}", (Convert.ToInt32(new Random().Next(7) + 1).ToString()));
                //                quote = quote.Replace("{0}+{random}", (Convert.ToInt32(u.DaysLeftToORD) + random).ToString());
                //                GlobalConfig.UserList[index].ORDDate.AddDays(random);

                //                quote = quote.Replace("{0}", GlobalConfig.UserList[index].DaysLeftToORD);
                //                Activity reply = GlobalConfig.UserList[index].UserAcitivity.CreateReply(quote);
                //                connector.Conversations.ReplyToActivity(reply);
                //            }

                //        }

                //    });
                //    FirebaseDataManager.UpdateAllUsersArr();
                //    GlobalConfig.Setup();
                //}
                //catch (Exception ex)
                //{
                //    ORDDiagnostic.SendDiagnostic(ex);
                //}

            }
        }
    }
}
