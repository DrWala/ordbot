﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ORDBot.Config;
using ORDBot.Diagnostic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ORDBot.Firebase
{
    public static class FirebaseDataManager
    {
        public static bool AddUser()
        {

            return false;
        }
        public static async Task GetAllUsers()
        {
            try
            {
                GlobalConfig.UserList = (await GetArrayFromFirebase("/Users.json")).ToObject<List<User>>();
            }
            catch (Exception ex)
            {
                ORDDiagnostic.SendDiagnostic(ex);
            }
        }
        public static async Task UpdateAllUsersArr()
        {
            string queryUrl = GlobalConfig.BaseUrl + "/Users.json";
            await new HttpClient().PutAsync(queryUrl, new StringContent(JsonConvert.SerializeObject(GlobalConfig.UserList.ToArray())));
        }
        public static async void GetAllBeforeQuotes()
        {
            GlobalConfig.QuotesBefore = (await GetArrayFromFirebase("/Quotes/Before.json")).ToObject<List<string>>();
        }
        public static async void GetAllDuringQuotes()
        {
            GlobalConfig.QuotesDuring = (await GetArrayFromFirebase("/Quotes/During.json")).ToObject<List<string>>();
        }
        public static async void GetAllAfterQuotes()
        {
            GlobalConfig.QuotesAfter = (await GetArrayFromFirebase("/Quotes/After.json")).ToObject<List<string>>();
        }
        private static async Task<JArray> GetArrayFromFirebase(string path)
        {
            try
            {
                string jsonResponse = null;
                path = GlobalConfig.BaseUrl + path;
                await new HttpClient().GetAsync(path)
                    .ContinueWith(async (taskwithresponse) =>
                    {
                        jsonResponse = await taskwithresponse.Result.Content.ReadAsStringAsync();
                    });
                JArray toReturn = JArray.Parse(jsonResponse);
                return toReturn;
            }
            catch (Exception ex)
            {
                ORDDiagnostic.SendDiagnostic(ex);
            }
            return null;
        }
    }
}
