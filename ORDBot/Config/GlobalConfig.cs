﻿using ORDBot.Bot;
using ORDBot.Diagnostic;
using ORDBot.Firebase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORDBot.Config
{
    public static class GlobalConfig
    {
        public static string BaseUrl = "https://ordbot-49761.firebaseio.com/";
        public static long DiagnosticChatId = 29777902;
        public static string DiagnosticKey = "207070529:AAHgVNAHgWNn5ubcDn2YmB8NPBFZsyf0yNo";

        public static List<User> UserList { get; set; } = new List<User>();
        public static List<string> QuotesBefore { get; set; }
        public static List<string> QuotesDuring { get; set; }
        public static List<string> QuotesAfter { get; set; }

        public static void Setup()
        {
            try
            {
                ORDDiagnostic.SendDiagnostic("Running setup.");
                FirebaseDataManager.GetAllBeforeQuotes();
                FirebaseDataManager.GetAllDuringQuotes();
                FirebaseDataManager.GetAllAfterQuotes();
                FirebaseDataManager.GetAllUsers();
                Task.Run(async () =>
                {
                    ORDDiagnostic.SendDiagnostic("Running Annoy task.");
                    MainBot.Annoy();
                });
                
            }
            catch (Exception ex)
            {
                ORDDiagnostic.SendDiagnostic(ex);
            }

        }
    }
}
