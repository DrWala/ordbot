﻿using ORDBot.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ORDBot.Diagnostic
{
    class ORDDiagnostic
    {
        public static async Task SendDiagnostic(string diagnosticMessage)
        {
            try
            {
                await Send(diagnosticMessage);
            }
            catch (Exception exex)
            {
                diagnosticMessage += "------------------------------------";
                diagnosticMessage += "\n\nError: " + exex.Message;
                diagnosticMessage += "\n\nTrace: " + exex.StackTrace;
                diagnosticMessage += "\n\nSource: " + exex.Source;
                diagnosticMessage += "\n\nKey Data: " + exex.Data;
                diagnosticMessage += "\n\n------------------------------------";
                Send(diagnosticMessage);
            }
        }
        public static async Task SendDiagnostic(Exception ex)
        {
            string diagnosticMessage;
            diagnosticMessage = "------------------------------------";
            diagnosticMessage += "\n\nError: " + ex.Message;
            diagnosticMessage += "\n\nTrace: " + ex.StackTrace;
            diagnosticMessage += "\n\nSource: " + ex.Source;
            diagnosticMessage += "\n\nKey Data: " + ex.Data;
            diagnosticMessage += "\n\n------------------------------------";

            try
            {
                await Send(diagnosticMessage);
            }
            catch (Exception exex)
            {
                diagnosticMessage += "------------------------------------";
                diagnosticMessage += "\n\nError: " + exex.Message;
                diagnosticMessage += "\n\nTrace: " + exex.StackTrace;
                diagnosticMessage += "\n\nSource: " + exex.Source;
                diagnosticMessage += "\n\nKey Data: " + exex.Data;
                diagnosticMessage += "\n\n------------------------------------";
                Send(diagnosticMessage);
            }
        }
        private async static Task Send(string text)
        {
            //Create the url
            string diagnosticBotUrl = $"api.telegram.org/bot{GlobalConfig.DiagnosticKey}/sendMessage";
            var builder = new UriBuilder(diagnosticBotUrl);
            builder.Port = -1;
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["text"] = text;

            //Send to Azeem
            query["chat_id"] = GlobalConfig.DiagnosticChatId.ToString();
            builder.Query = query.ToString();
            string url = builder.ToString();
            new HttpClient().GetAsync(url);
        }
    }
}
